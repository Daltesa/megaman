move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x+= sign(hsp);
    }
    hsp = 0;
    hcollision = true;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;

//ground collision
if (place_meeting (x, y+8, obj_wall))
{
    
    if (fearofheights && !position_meeting
    (x+(sprite_width/2)*dir, y+(sprite_height/2)+8, obj_wall))
    {
     if(place_meeting (x+40*dir, y+(sprite_height/2)+8, obj_wall)){
            vsp = -3;
            y+= vsp;
     }
     else{
        dir *= -1;
     }
        
        
    }
}

